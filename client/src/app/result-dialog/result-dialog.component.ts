import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
  selector: 'app-result-dialog',
  templateUrl: './result-dialog.component.html',
  styleUrls: ['./result-dialog.component.less']
})
export class ResultDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) private data: any) {
  }

  public _resultString: string = '';

  ngOnInit(): void {
    if (this.data.result === '') {
      this._resultString = 'Input Error'
    } else {
      this._resultString = this.data.expression + ' = ' + this.data.result;
    }
  }
}

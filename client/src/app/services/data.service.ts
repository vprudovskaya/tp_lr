import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";

@Injectable({providedIn: "root"})
export class DataService {

  constructor(private http: HttpClient) {
  }

  public postArithmeticExpression(expression: string): Observable<string> {
    return this.http.post<string>('http://localhost:5000/api/parser', {expression: expression});
  }

  public getHistory(): Observable<any> {
    return this.http.get('http://localhost:5000/api/logger');
  }
}


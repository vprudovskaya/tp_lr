export default class Expression {
  expression;
  result;

  constructor(expression: string | null, result: string | null) {
    this.expression = expression;
    this.result = result
  }
}

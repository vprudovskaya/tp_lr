import Expression from './expression.model'
import {Component, DoCheck, OnInit} from '@angular/core';
import {DataService} from './services/data.service';
import {ResultDialogComponent} from "./result-dialog/result-dialog.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  title = 'client';
  postData = '';
  previousExpressions = new Array<Expression>();


  constructor(private dataService: DataService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.dataService.getHistory().subscribe(
      resp => {
        resp.history.forEach((record: any) => {
          this.previousExpressions.push(record);
        });
      });
  }

  postExpression() {
    this.dataService.postArithmeticExpression(this.postData).subscribe(
      resp => {
        this.showResultDialog(this.postData, resp);
        this.saveData(this.postData, resp);
        this.postData = '';
      },
      error => {
        this.showResultDialog(this.postData, '');
        this.saveData(this.postData, 'Input Error');
        this.postData = '';
      }
    )
  }

  private saveData(expression: string, result: string): void {
    this.previousExpressions.push({expression: expression, result: result});
  }

  private showResultDialog(expression: string, result: string): void {
    this.dialog.open(ResultDialogComponent, {
      height: '150px',
      width: '300px',
      data: {
        expression,
        result
      }
    });
  }
}

const fs = require("fs");
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const jpv = require("jpv");

app.use(cors());
app.use(bodyParser.json());


const historyPattern = {
    history : [{
        expression: "(string)",
        result: jpv.or("(number)","(string)")
    }]
};

let result;
let historyArray = [];

app.post('/api/parser', (req, res) => {
    let expression = req.body.expression;
    try {
        result = eval(expression);
        if (isNaN(result)) {
            res.status(400).json('Input Error');
            writeToTheFile({expression: req.body.expression, result: 'Input Error'});
        } else {
            res.status(200).json(result);
            writeToTheFile({expression: req.body.expression, result: result});
        }
    } catch (error) {
        res.status(400).json('Input Error');
        writeToTheFile({expression: req.body.expression, result: 'Input Error'});
    }
});

let writeToTheFile = (object) => {
    historyArray.push(object);
    let fileContext = {
        history: historyArray
    };
    fs.writeFileSync("../history.json", JSON.stringify(fileContext));
};

let isValid = (history) => {
    return jpv.validate(JSON.parse(history), historyPattern);
}

app.get('/api/logger', (req, res) => {
    const history = fs.readFileSync('../history.json', 'utf8');

    if (isValid(history)) {
        JSON.parse(history).history.forEach(record => {
            historyArray.push(record);
        });
        res.status(200).json(JSON.parse(history));
    } else {
        res.status(500).json('Server Error');
    }
});

module.exports = app;
